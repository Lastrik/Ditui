'use-strict';

const config = require('./config'),
	Ctrl = require('./ctrl/Ctrl'),
	Wrk = require('./wrk/Wrk'),
	UI = require('./ui/UI'),
	ctrl = new Ctrl(config),
	wrk = new Wrk(config),
	ui = new UI(config, 'Ditui', 'Choose a Guild', 'Private Channels');

ctrl.ui = ui;
ctrl.wrk = wrk;

ui.ctrl = ctrl;
wrk.ctrl = ctrl;

ctrl.launch();
