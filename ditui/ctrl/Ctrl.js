'use-strict';

const dateFormat = require('dateformat');

class Ctrl {

	constructor(config) {
		this.guilds = null;
		this.canFetch = false;
		this.config = config;
	}

	launch() {
		this._setupListeners();
		this.loading('Connecting to the Discord API...');
		this.wrk.login();
	}

	loading(text) {
		this.ui.loading(text);
	}

	showMain() {
		this.ui.showMain();
	}

	setChannels(chans) {
		this.ui.leftMenuData = chans;
	}

	changeChannel(guildName, chanName) {
		this.ui.infoBoxTitle = `{${this.config.color.accent}-fg}{bold}${this._esc(guildName)}{/} | ${this._esc(chanName)}`;
	}

	addMessage(msg, author) {
		this.ui.addLineChat(this._parseMessage(msg));
	}

	addMessages(messages) {
		this.canFetch = false;
		var lines = [];
		for(let msg of messages) {
			lines.push(this._parseMessage(msg));
		}
		this.ui.addMultipleLinesChat(lines);
		this.canFetch = true;
	}	

	addMessagesBefore(messages) {
		this.canFetch = false;
		var lines = [];
		for(let msg of messages) {
			lines.push(this._parseMessage(msg));
		}
		this.ui.addMultipleLinesBeforeChat(lines);
		this.canFetch = true;
	}

	_setupListeners() {
		this.ui.setupListeners();
		this.wrk.setupListeners();
		
		this.ui.on('exit', () => this.wrk.exit());
		this.ui.key(this.config.keys.guildMenu, () => this._openGuildMenu());
		
		this.ui.on('chatSubmit', (content) => {
			this.wrk.sendMsg(content);
		});

		this.ui.on('leftMenuSelect', el => {
			if(el.connect) {
				if(!el.connected) this.wrk.connectVoice(el.parent.channel);
				else this.wrk.disconnectVoice(el.parent.channel);
			} else if(el.channel) this.wrk.setCurrentChannel(el.channel);
		});

		this.ui.on('promptSelect', (el, index) => {
			this.wrk.setCurrentGuild(this.guilds[index]);
		});

		this.ui.on('promptBtnSelect', () => {
			this.wrk.privateChannels();
		});

		this.ui.on('scrolledTop', () => {
			if(this.canFetch) {
				this.canFetch = false;
				this.wrk.fetchMoreMessages();
			}
		});
	}

	_toDate(timestamp) {
		var date = new Date(timestamp);
		return dateFormat(date, this.config.dateFormat);
	}

	_esc(text) {
		return this.ui.escape(text);
	};

	_openGuildMenu() {
		this.guilds = this.wrk.guilds;
		var items = this.guilds.map(guild => guild.name);
		this.ui.openPrompt(items);
	}

	_parseMessage(msg) {
		var color = this.config.color.secondary;
		var name = msg.author.username;
		if(msg.member) {
			color = msg.member.displayHexColor;
			name = msg.member.displayName;
		} else if (msg.author === msg.client.user) {
			color = this.config.color.accent;
		}
		var date = this._toDate(msg.createdTimestamp);
		var prefix = `{${color}-fg}{bold}${date+this._esc(name)}:{/} `
		var res = prefix + this._esc(msg.cleanContent);
		if(msg.attachments.array().length > 0) {
			if(msg.cleanContent) res += '\n' + prefix;
			res += msg.attachments.array().map(att => att.url).join('\n' + prefix);
		}
		return res;
	}

}

module.exports = Ctrl;
