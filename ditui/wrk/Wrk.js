'use-strict';

const Discord = require('discord.js'),
	Notifier = require('./Notifier'),
	VoiceManager = require('./VoiceManager');

class Wrk {

	constructor(config) {
		this.config = config;
		this.client = new Discord.Client();
		this.voiceMan = new VoiceManager();
		this.notifier = new Notifier();
	}

	login() {
		this.client.login(this.config.token);
	}
	
	disconnectVoice(chan) {
		this.voiceMan.disconnect();
		chan.leave();
	}

	connectVoice(chan) {
		chan.join().then(conn => {
			this.voiceMan.connect(conn);
		})
		.catch(err => {});
	}

	privateChannels() {
		this.curGuild = null;
		var tree = {Privates : {extended: true, children: {}}, Groups: {extended: true, children: {}}};
		var dm = this.client.channels.findAll('type', 'dm');
		for(let chan of dm) {
			chan.name = chan.recipient.username;
			tree.Privates.children[chan.name] = {channel: chan};
		}
		var group = this.client.channels.findAll('type', 'group');
		for(let chan of group) {
			if(!chan.name) {
				chan.name = chan.recipients.array().map(u => u.username).join(' - ');
				tree.Groups.children[chan.name] = {channel: chan};
			}
		}
		this.ctrl.setChannels(tree);
		this.setCurrentChannel(dm[0]);
	}

	sendMsg(msg) {
		if(msg) this.curChan.send(msg).catch(console.error);
	}

	fetchMoreMessages() {
		if(this.lastMsgSnowflake) {
			this.curChan.fetchMessages({before: this.lastMsgSnowflake})
				.then(messages => {
					this.lastMsgSnowflake = messages.lastKey();
					this.ctrl.addMessagesBefore(messages.array());
				})
				.catch(console.error);
		}
	};

	exit() {
		this.client.destroy();
		return process.exit(0)
	}

	setupListeners() {
		this.client.on('ready', () => {
			this.ctrl.showMain();
			this.setCurrentGuild(this.client.guilds.array()[0]);
		});

		this.client.on('message', (msg) => {
			if(msg.channel === this.curChan) {
				this.ctrl.addMessage(msg);
			}
			this.notifier.checkAndNotify(msg, this.client.user, this.curChan);
		});

		this.client.on('voiceStateUpdate', (old, member) => {
			if(member.guild === this.curGuild && old.voiceChannel != member.voiceChannel) {
				this._updateChanList();
			}
		});
	}

	setCurrentGuild(guild) {
		this.curGuild = guild;
		this._updateChanList();
	}

	setCurrentChannel(chan) {
		if(this.curChan !== chan) {
			this.curChan = chan;
			var guildName = chan.guild ? chan.guild.name : 'Private Channel';
			this.ctrl.changeChannel(guildName, chan.name);
			chan.fetchMessages()
				.then(messages => {
					this.lastMsgSnowflake = messages.lastKey();
					this.ctrl.addMessages(messages.array());
				})
				.catch(console.error);
		}
	}

	get guilds() {
		return this.client.guilds.findAll('available', true);
	}

	_updateChanList() {
		var chans = this.curGuild.channels
			.filter(chan => {
				return (chan.type == 'text' && chan.permissionsFor(chan.guild.me).has(Discord.Permissions.FLAGS.VIEW_CHANNEL))
					|| (chan.type == 'voice' && chan.permissionsFor(chan.guild.me).has(Discord.Permissions.FLAGS.CONNECT));
			}).array();
		chans = chans.sort(function(a, b) {
			if(a.parent && !b.parent || a.type == 'voice' && b.type == 'text') return 1;
			if(!a.parent && b.parent || a.type == 'text' && b.type == 'voice') return -1;
			return a.position > b.position;
		});
		
		var chanTree = {};
		for(let chan of chans) {
			let prefix = chan.type == 'text' ? '# ' : '$ ';
			var chanNode = {channel: chan};
			if(chan.parent) {
				if(!chanTree[chan.parent.name]) {
					chanTree[chan.parent.name] = {category: chan.parent, extended: true, children: {}};
				}
				chanTree[chan.parent.name].children[prefix + chan.name] = chanNode;
			} else {
				chanTree[prefix + chan.name] = chanNode;
			}
			if(chan.type == 'voice' && chan.members) {
				let connected = false;
				chanNode.children = {connect: {}};
				for(let member of chan.members.array()) {
					if(member.user == this.client.user) connected = true;
					chanNode.children[member.displayName] = {member: member} 
				}
				chanNode.children.connect.name = connected ? '-> Disconnect <-' : '-> Connect <-';
				chanNode.children.connect.connect = true;
				chanNode.children.connect.connected = connected;
			} 
		}
		
		this.setCurrentChannel(chans[0]);
		this.ctrl.setChannels(chanTree);
	}

}

module.exports = Wrk;
