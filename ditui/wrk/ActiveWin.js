'use-strict';

const activeWin = require('active-win');

class ActiveWin {

	constructor() {
		this.pid = null;
	}

	setup() {
		(async() => {
			this.pid = (await activeWin()).id;
		})();
	}

	isActiveWin(cb) {
		(async () => {
			cb(this.pid === (await activeWin()).id);
		})();
	}

}

module.exports = ActiveWin;
